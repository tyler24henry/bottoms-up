package com.example.bottomsup.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.view.category.CategoryDrinkState
import com.example.bottomsup.view.category.CategoryState
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }
    private val _state = MutableLiveData(CategoryState(isLoading = true))
    val state: LiveData<CategoryState> get() = _state

    private val _categoryDrinkState = MutableLiveData(CategoryDrinkState(isLoading = true))
    val categoryDrinkState: LiveData<CategoryDrinkState> get() = _categoryDrinkState

    init {
        viewModelScope.launch {
            val categoryDTO = repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO.categoryItems)
        }
    }

    fun getCategoryDrinks(category: String) {
        viewModelScope.launch {
            val categoryDrinkDTO = repo.getCategoryDrinks(category)
            _categoryDrinkState.value = CategoryDrinkState(categoryDrinks = categoryDrinkDTO.drinks)
        }
    }
}