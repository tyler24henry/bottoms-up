package com.example.bottomsup.viewmodel

import androidx.lifecycle.*
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.view.category.CategoryDrinkState
import kotlinx.coroutines.launch

class CategoryDrinkViewModel(category: String) : ViewModel() {
    private val repo by lazy { BottomsUpRepo }
    val state : LiveData<CategoryDrinkState> = liveData {
        emit(CategoryDrinkState(isLoading = true))
        // don't need viewModelScope.launch because already in courotine scope
        val categoryDrinks = repo.getCategoryDrinks(category)
        emit(CategoryDrinkState(categoryDrinks = categoryDrinks.drinks))
    }

    // instead of doing all this below, we moved the code into the liveData scope
//    private val _state = MutableLiveData(CategoryDrinkState(isLoading = true))
//    val state: LiveData<CategoryDrinkState> get() = _state
//
//    init {
//        viewModelScope.launch {
//            val categoryDrinks = repo.getCategoryDrinks(category)
//            _state.value = CategoryDrinkState(categoryDrinks = categoryDrinks.drinks)
//        }
//    }
}





























