package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.view.drinkDetails.DrinkDetailsState

class DrinkDetailsViewModel(id: Int) : ViewModel() {
    private val repo by lazy { BottomsUpRepo }
    val state: LiveData<DrinkDetailsState> = liveData {
        // set initial loading state
        emit(DrinkDetailsState(isLoading = true))
        // fetch data
        val drinkDetails = repo.getDrinkDetails(id)
        // when we get data from the server, set state
        emit(DrinkDetailsState(drinkDetails = drinkDetails.drinks))
    }
}