package com.example.bottomsup.view.drinkDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.bottomsup.databinding.FragmentDrinkDetailsBinding
import com.example.bottomsup.viewmodel.DrinkDetailsViewModel

class DrinkDetailsFragment : Fragment() {
    private var _binding: FragmentDrinkDetailsBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinkDetailsFragmentArgs>()
    private val drinkDetailsViewModel by viewModels<DrinkDetailsViewModel>() {
        DrinkDetailsViewModelFactory(args.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailsBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailsViewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.drinkDetails.isNotEmpty()) {
                with(binding) {
                    val drink = state.drinkDetails[0]
                    tvDrink.text = drink.strDrink
                    ivDrink.load(drink.strDrinkThumb)
                    tvDrinkId.text = drink.idDrink
                }

            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}