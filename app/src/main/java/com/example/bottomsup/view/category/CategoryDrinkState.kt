package com.example.bottomsup.view.category

import com.example.bottomsup.model.response.CategoryDrinksDTO

data class CategoryDrinkState(
    val isLoading: Boolean = false,
    val categoryDrinks: List<CategoryDrinksDTO.Drink> = emptyList()
)
