package com.example.bottomsup.view.drinkDetails

import com.example.bottomsup.model.response.DrinkDetailsDTO

data class DrinkDetailsState(
    val isLoading: Boolean = false,
    val drinkDetails: List<DrinkDetailsDTO.Drink> = emptyList()
)
