package com.example.bottomsup.view.drinkDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bottomsup.viewmodel.DrinkDetailsViewModel

class DrinkDetailsViewModelFactory(private val id: Int) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DrinkDetailsViewModel(id) as T
    }
}